FROM ubuntu:23.04 AS base
ENV DEBIAN_FRONTEND=noninteractive
ENV PYODIDE_VERSION=0.23.4
RUN apt-get update && apt-get -y upgrade
RUN apt-get update && apt-get -y install \
        nodejs \
        ccache \
        python3-pip \
        python3 \
        git \
        make \
        pkg-config \
        g++ \
        lbzip2 \
        xz-utils \
        autoconf \
        libtool \
        unzip \
        tree \
        xxd \
        cmake \
        zip \
        wget

WORKDIR /root
RUN git clone https://github.com/emscripten-core/emsdk.git
RUN cd emsdk && ./emsdk install latest && ./emsdk activate latest
RUN wget https://github.com/pyodide/pyodide/releases/download/${PYODIDE_VERSION}/xbuildenv-${PYODIDE_VERSION}.tar.bz2
RUN tar xvf xbuildenv-${PYODIDE_VERSION}.tar.bz2
RUN rm xbuildenv-${PYODIDE_VERSION}.tar.bz2
RUN ls
RUN mkdir /root/output
WORKDIR /root/output
RUN wget https://github.com/pyodide/pyodide/releases/download/${PYODIDE_VERSION}/pyodide-core-${PYODIDE_VERSION}.tar.bz2
RUN tar xvf pyodide-core-${PYODIDE_VERSION}.tar.bz2
RUN rm pyodide-core-${PYODIDE_VERSION}.tar.bz2
RUN ln -s /usr/bin/python3 /usr/bin/python
RUN rm /usr/lib/python3.11/EXTERNALLY-MANAGED

FROM docker.gitlab.gwdg.de/ngsuite/ngsx-pyodide:base AS occbuild
WORKDIR /root/
RUN git clone https://github.com/ngsolve/ngsolve
WORKDIR /root/output
RUN bash /root/ngsolve/tests/gitlab-ci/pyodide/build_opencascade.sh
RUN rm -rf /root/ngsolve 

FROM docker.gitlab.gwdg.de/ngsuite/ngsx-pyodide:occbuild AS ngsolvebuild
#COPY --from=docker.gitlab.gwdg.de/ngsuite/ngsx-pyodide:occbuild /opt/opencascade /opt/opencascade
WORKDIR /root/
RUN git clone https://github.com/ngsolve/ngsolve
# COPY --from=occbuild /root/ngsolve /root/ngsolve
WORKDIR /root/ngsolve
RUN git submodule update --init --recursive
RUN mkdir /root/ngsolve/build
WORKDIR /root/ngsolve/build
RUN --mount=type=cache,target=/ccache/ bash /root/ngsolve/tests/gitlab-ci/pyodide/build_in_docker.sh
WORKDIR /root/
RUN rm -rf /root/ngsolve /root/OCCT* /root/V*.zip 

# NEW, ngsxfem stuff
#FROM ngsolvebuild AS ngsxfembuild
FROM docker.gitlab.gwdg.de/ngsuite/ngsx-pyodide:ngsolvebuild AS ngsxfembuild
#COPY --from=docker.gitlab.gwdg.de/ngsuite/ngsx-pyodide:ngsolvebuild /opt/netgen /opt/netgen
WORKDIR /root/
RUN git clone https://github.com/ngsxfem/ngsxfem --branch master
#https://gitlab.gwdg.de/ngsuite/ngsxfem.git
WORKDIR /root/ngsxfem/
ADD ngsxfem /root/ngsxfem-pyodide
RUN mkdir /root/ngsxfem/build
WORKDIR /root/ngsxfem/build
RUN --mount=type=cache,target=/ccache/ bash /root/ngsxfem-pyodide/build_in_docker.sh
WORKDIR /root/
RUN rm -rf /root/ngsxfem

# NEW, ngstrefftz stuff
FROM docker.gitlab.gwdg.de/ngsuite/ngsx-pyodide:ngsolvebuild AS ngstrefftzbuild
#COPY --from=docker.gitlab.gwdg.de/ngsuite/ngsx-pyodide:ngsolvebuild /opt/netgen /opt/netgen
WORKDIR /root/
RUN git clone https://github.com/PaulSt/NGSTrefftz ngstrefftz --branch main
WORKDIR /root/ngstrefftz/
RUN git submodule update --init --recursive
ADD ngstrefftz /root/ngstrefftz-pyodide
RUN mkdir /root/ngstrefftz/build
WORKDIR /root/ngstrefftz/build
RUN --mount=type=cache,target=/ccache/ bash /root/ngstrefftz-pyodide/build_in_docker.sh
WORKDIR /root/
RUN rm -rf /root/ngstrefftz

FROM docker.gitlab.gwdg.de/ngsuite/ngsx-pyodide:base AS pyodide
COPY --from=docker.gitlab.gwdg.de/ngsuite/ngsx-pyodide:ngsolvebuild /opt/netgen/python/pyngcore /opt/netgen/python/pyngcore
COPY --from=docker.gitlab.gwdg.de/ngsuite/ngsx-pyodide:ngsolvebuild /opt/netgen/python/netgen /opt/netgen/python/netgen
COPY --from=docker.gitlab.gwdg.de/ngsuite/ngsx-pyodide:ngsolvebuild /opt/netgen/python/ngsolve /opt/netgen/python/ngsolve
COPY --from=docker.gitlab.gwdg.de/ngsuite/ngsx-pyodide:ngsxfembuild /opt/netgen/python/xfem /opt/netgen/python/xfem
COPY --from=docker.gitlab.gwdg.de/ngsuite/ngsx-pyodide:ngstrefftzbuild /opt/netgen/python/ngstents /opt/netgen/python/ngstents
COPY --from=docker.gitlab.gwdg.de/ngsuite/ngsx-pyodide:ngstrefftzbuild /opt/netgen/python/ngstrefftz /opt/netgen/python/ngstrefftz

WORKDIR /root/output/pyodide
RUN rm /opt/netgen/python/*/*.a
WORKDIR /opt/netgen/python
RUN zip -r pyngcore.zip pyngcore
RUN zip -r netgen.zip netgen
RUN zip -r ngsolve.zip ngsolve
RUN zip -r xfem.zip xfem
RUN zip -r ngstrefftz.zip ngstrefftz
RUN zip -r ngstents.zip ngstents
RUN cp /opt/netgen/python/*.zip /root/output/pyodide
WORKDIR /root/output/pyodide
ADD generate_repodata.js /root/output/pyodide
#COPY --from=occbuild /root/ngsolve/tests/gitlab-ci/pyodide/generate_repodata.js /root/output/pyodide
RUN node generate_repodata.js
RUN rm -rf *.whl
ADD merge.py /root/
RUN python3 /root/merge.py
RUN cd /root/output/ && tar -cvjSf ngsx_pyodide.tar.bz2 pyodide
