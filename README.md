# Setting up Pyodide for with NGSolve + Addons

This repo contains the scripts to build a Pyodide package with NGSolve and its addons in docker.

## Setup of the CI pipeline

The CI pipeline is configured to build the package in several steps and upload it to a webserver. All the steps are to be triggered manually. To reduce image sizes, each step is squashed into one docker layer. 

The final file can be downloaded from [here](https://lehrenfeld.pages.gwdg.de/ngsx-pyodide/ngsx_pyodide.tar.bz2).

Warning:
Each step -- except for the first one or the build-all-type steps -- rely on the docker images corresponding to previous steps in the [container registry](https://gitlab.gwdg.de/lehrenfeld/ngsx-pyodide/container_registry), i.e. if you trigger step C manually, it will rely on the image for step B in the container registry which may be older (inconsistent). So, consistency is within the responsibility of the user. If you want to be sure, you can use the build-all step or run every locally and upload the images to the container registry later.

## Adding your own package

To work on your own package you can start with the stages in the Dockerfile that deliver a properly (WASM-)compiled NGSolve (or the similarly for ngsxfem or ngstrefftz). You can then add your own compilation step as a stage. At the end the packaging (the `pyodide` stage in the Dockerfile) needs to be adjusted (this concerns the steps in the Dockerfile as well as the information in `merge.py`). 


